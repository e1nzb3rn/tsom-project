import '../sass/index.scss'
import Logo from '../img/logo.png'
import bgPromo from '../img/bg-promo.jpg'
import gal1 from '../img/Layer-1.png'
import gal2 from '../img/Layer-2.png'
import gal3 from '../img/Layer-3.png'
import gal4 from '../img/Layer-4.png'
import gal5 from '../img/Layer-6-4.png'
import footerBg from '../img/mountain.png'

// import { assetProd } from '../js/helper/env.js'

document.querySelector('#app').innerHTML = `
<header class="d-flex flex-wrap justify-content-center py-3 mb-4 border-bottom">
  <ul class="nav nav-pills center">
    <li class="nav-item"><a href="#" class="nav-link" aria-current="page">HOME</a></li>
    <li class="nav-item"><a href="#" class="nav-link">ABOUT US</a></li>
    <li class="nav-item"><a href="#" class="nav-link">SEATING PLAN</a></li>
    <li class="nav-item"><a href="#" class="nav-link">TICKETS</a></li>
    <li class="nav-item"><a href="#" class="nav-link">GALLERY</a></li>
    <li class="nav-item"><a href="#" class="nav-link">INFO</a></li>
    <li class="nav-item"><a href="#" class="nav-link">FAQ</a></li>
  </ul>
</header>
`

document.querySelector('.content').innerHTML = `
  <div class="main">

    <div class="d-flex flex-wrap justify-content-center">
      <img src="${Logo}" class="img-fluid" alt="tsom logo"/>
    </div>
  </div>
`

document.querySelector('.tsom--banner-desc').innerHTML = `
  <div class="tsom--banner-bg">
    <div class="tsom--banner-title">
      THIS ICONIC SHOW TOUCHES THE HEARTS OF ALL AGES - DON'T MISS IT!
    </div>
  </div>
`

document.querySelector('.tsom--content-top').innerHTML = `
  <div class="tsom--content-txt">
    <div class="row align-items-start">
      <div class="col-md-6 col-xs-12">
        <p>The world’s best-loved musical, THE SOUND OF MUSIC, comes to Jakarta-INDONESIA this 2015, with a dazzling production from the home of West End musical theatre, the famous London Palladium. This lavish and critically-acclaimed production tells the uplifting true story of Maria, the fun-loving governess who changes the lives of the widowed Captain Von Trapp and his seven children by re-introducing them to music, culminating in the family’s escape across the mountains.</p>
      </div>
      <div class="col-md-6 col-xs-12">
        <p>
          The original 1959 Broadway production of THE SOUND OF MUSIC won six Tony Awards, including Best Musical, and was Rodgers and Hammerstein’s biggest success. The 1965 movie starring Julie Andrews as Maria won five Oscars, including Best Picture, and remains one of the most popular movies of all time. THE SOUND OF MUSIC brims over with some of the most memorable songs ever performed on the musical theatre stage, including My Favorite Things, Do-Re-Mi, Climb Ev’ry Mountain, Edelweiss, The Lonely Goatherd, Sixteen Going on Seventeen, and of course the glorious title song The Sound of Music.
        </p>
      </div>
    </div>
  </div>
`

document.querySelector('.tsom--slider-banner').innerHTML = `
  <div class="d-flex flex-wrap">
    <img src="${bgPromo}" alt="tsom promo" />
  </div>
`

document.querySelector('.tsom--news-content').innerHTML = `
  <div class="tsom--content-news">
    <div class="tsom--content-news-title">
      <h2>NEWS</h2>
    </div>
    <p>
      Lorem ipsum dolor sit amet, consetetur sadipscing elitr,  sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem 
    </p>
    <p>
      ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr,  sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea taki
    </p>
    <p>
      mata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr,  sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit ame
    </p>
  </div>
`

document.querySelector('.tsom--news-ticket').innerHTML = `
  <div class="tsom--content-ticket">
    <div class="tsom--content-news-ticket-title">
      <h2>TICKETS</h2>
    </div>
    <p>
      lorem ipsum dolor sit amet, consetetur sadipscing elitr,  sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor 
    </p>
  </div> 
`

document.querySelector('.tsom--gallery-content').innerHTML = `
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="tsom--gallery-img-first">
          <img src="${gal1}" class="img-fluid" alt="image first" />
          <figcaption>
            Jakarta Concert, 21 March 2010
          </figcaption>
          <p>
            Lorem ipsum dolor sit amet, consetetur sadipscing elitr,  sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr,  sed diam nonumy eirmod tempor invidunt ut labore et dolore 
          </p>
        </div>
      </div>
      <div class="col-md-6">
        <div class="row tsom--gallery-list">
          <div class="col">
            <img src="${gal2}" class="img-thumbnail alt="image second" /> 
            <img src="${gal3}" class="img-thumbnail alt="image second" /> 
            <img src="${gal5}" class="img-thumbnail alt="image second" /> 
          </div>
          <div class="col">
            <img src="${gal4}" class="img-thumbnail alt="image second" /> 
            <img src="${gal5}" class="img-thumbnail alt="image second" /> 
            <img src="${gal2}" class="img-thumbnail alt="image second" /> 
          </div>
        </div>
      </div>
    </div>
  </div>
`

document.querySelector(".tsom--footer").innerHTML = `
  <div class="tsom--footer-bg">
    <div class="row">
      <img src="${footerBg}" class="img-fluid alt="image footer backaground" /> 
    </div>
  </div>
`

